from PIL import Image
import hashlib

def dhash(image, hash_size = 8):
    image = image.convert('L').resize((hash_size,hash_size+1),Image.ANTIALIAS)
    pixels = list(image.getdata())
    
    #Compare adjacent pixels
    diff = []
    for row in range(0,hash_size):
        for col in range(0,hash_size):
            p_left = image.getpixel((row,col))
            p_right = image.getpixel((row,col+1))
            diff.append(p_left>p_right)
    dec_val = 0
    hex_string = []
    for index, value in enumerate(diff):
        if value:
            dec_val +=2**(index%8)
        if index%8==7:
            hex_string.append(hex(dec_val)[2:].rjust(2,'0'))
            dec_val = 0
        print(dec_val)
    print("409827502894375932468759324658792436897563284658972364392834765987234689276")
        
    return ''.join(hex_string)

def nearSimilar(image1, image2):
    a=dhash(Image.open(image1))
    b=dhash(Image.open(image2))
    print(a)
    print(b)
    print(a==b)



nearSimilar("doc.jpg","doc2.jpg")
    



