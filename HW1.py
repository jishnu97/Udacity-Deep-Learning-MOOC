"""Lesson 1:
Logistic regression is a supervised classifier. It is a linear classifier that uses a weight and a bias to predict the output. The output score is converted into probability using a function known as softmax function.

Multinomial logistic classification is when the probabilities generated form the logics are compared to the one hot encoded vector to see the confidence/performance of the model.


The average cross entropy is the training loss and is measured by finding the average of distances for all possible labels.
"""
"""Softmax."""

scores = [1.0, 2.0, 3.0]

import numpy as np
def softmax(x):
    
    return np.exp(x)/sum(np.exp(x))


print(softmax(scores * 10))

# Plot softmax curves
import matplotlib.pyplot as plt
x = np.arange(-2.0, 6.0, 0.1)
scores = np.vstack([x, np.ones_like(x), 0.2 * np.ones_like(x)])

plt.plot(x, softmax(scores * 10).T, linewidth=2)
plt.show()





#----------------------------------------------------------------------------------

#High error
a=  1000000000
for i in range(0,1000000):
    a+=0.000001
a-=1000000000
print(a)

#Low error
a=  1
for i in range(0,1000000):
    a+=0.000001
a-=1
print(a)

#This is why normalizing is important
#It also makes optimizing easier

#----------------------------------------------------------------------------------
#It is better to start with unsure distributions and increase confidence slowly
#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------
